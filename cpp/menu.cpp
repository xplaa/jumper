#include "../include/menu.h"
#include "iostream"
#include <sstream>
#include <string>
#include <fstream>

using namespace std;
int score;
string str1;
stringstream ss1;
string str2;
stringstream ss2;
string stDbest;

Cmenu::Cmenu()
{

    texture.loadFromFile("sprite/Clouds1.png");
    sprite.setTexture(texture);
    sprite.setPosition(0,0);


    font.loadFromFile("south park.ttf");

    name.setFont(font);
    name.setCharacterSize(95);
    name.setPosition(5,10);
    name.setColor(sf::Color::Yellow);
    name.setStyle(sf::Text::Bold);
    name.setString("JUMPER");

    start.setFont(font);
    start.setCharacterSize(35);
    start.setPosition(15,200);
    start.setColor(sf::Color::Green);
    start.setStyle(sf::Text::Bold);
    start.setString("Start new game");

    exit.setFont(font);
    exit.setCharacterSize(35);
    exit.setPosition(15,300);
    exit.setColor(sf::Color::Blue);
    exit.setStyle(sf::Text::Bold);
    exit.setString("Exit");

    textGameOver.setFont(font);
    textGameOver.setCharacterSize(65);
    textGameOver.setPosition(7,100);
    textGameOver.setColor(sf::Color::Red);
    textGameOver.setStyle(sf::Text::Bold);
    textGameOver.setString("Game Over");

    textScore.setFont(font);
    textScore.setCharacterSize(25);
    textScore.setPosition(7,200);
    textScore.setColor(sf::Color::Yellow);
    textScore.setStyle(sf::Text::Bold);
    textScore.setString("Your score: ");

    best.setFont(font);
    best.setCharacterSize(25);
    best.setPosition(7,250);
    best.setColor(sf::Color::Yellow);
    best.setStyle(sf::Text::Bold);


    restart.setFont(font);
    restart.setCharacterSize(35);
    restart.setPosition(100,350);
    restart.setColor(sf::Color::Green);
    restart.setStyle(sf::Text::Bold);
    restart.setString("Restart");

    exit2.setFont(font);
    exit2.setCharacterSize(35);
    exit2.setPosition(140,400);
    exit2.setColor(sf::Color::Blue);
    exit2.setStyle(sf::Text::Bold);
    exit2.setString("Exit");

    smallScore.setFont(font);
    smallScore.setCharacterSize(25);
    smallScore.setPosition(5,10);
    smallScore.setColor(sf::Color::Yellow);
    // smallScore.setStyle(sf::Text::Bold);


    //menu = false;
    menu = true;
    select1 = true;
    select2 = true;

    buffer_menu.loadFromFile("audio/main_theme.ogg");
    sound_menu.setBuffer(buffer_menu);
    sound_menu.setLoop(true);
    sound_menu.setVolume(30);
    sound_menu.play();
    Read();

    best.setString("Best:" +stDbest);
}

void Cmenu::Update()
{
    if (!select1)
    {
        exit.setColor(sf::Color::Green);
        start.setColor(sf::Color::Blue);

    }
    if (select1)
    {
        exit.setColor(sf::Color::Blue);
        start.setColor(sf::Color::Green);
    }
    if (select2)
    {
        exit2.setColor(sf::Color::Blue);
        restart.setColor(sf::Color::Green);
    }
    if (!select2)
    {
        exit2.setColor(sf::Color::Green);
        restart.setColor(sf::Color::Blue);
    }


}

void Cmenu::Update_score()
{
    ss1.str(std::string());
    ss1 << score;
    str1 = ss1.str();
    textScore.setString("Your score: "+ str1);
    smallScore.setString("score: "+ str1);
}

void Cmenu::Read()
{
    ifstream F;
    F.open("test.txt");

    while (!F.eof())
    {
        F>>Dbest;
    }
    ss2.str(std::string());
    ss2 << Dbest;
    stDbest = ss2.str();
    cout<<stDbest<<" stDbest"<<endl;
    F.close();

}


void Cmenu::Write()
{
    if (Dbest < score)
    {
        cout<<"write"<<endl;
        ss2.str(std::string());
        ss2 << score;
        str2 = ss2.str();
        best.setString("Best: "+ str2);
        ofstream F;
        F.open("test.txt");
        F<<score;
    }
}

