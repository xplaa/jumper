#include "..\include\Player.h"
#include <SFML/Audio.hpp>
#include<iostream>
using namespace std;
extern int score;

sf::SoundBuffer buffer_jump;
sf::Sound sound_jump;

CPlayer:: CPlayer()
{
    Texture_Player.loadFromFile("sprite/soldier_1.png");
    setTexture(Texture_Player);
    setPosition(150,250);

    bottom = getPosition().y + getLocalBounds().height;
    left = getPosition().x;
    right = getPosition().x + getLocalBounds().width;
    top = getPosition().y;

    Rect.left = left;
    Rect.top = top;
    Rect.width = right - left;
    Rect.height = bottom - top;

    buffer_jump.loadFromFile("audio/Jump.wav");
    sound_jump.setBuffer(buffer_jump);
    sound_jump.setVolume(15);

}

CPlayer:: ~CPlayer()
{

}

void CPlayer:: Update(sf::Time deltaTime)
{
    possitionX = getPosition().x;
    possitionY = getPosition().y;
    adjust(deltaTime);
    if(!isJump)
    {
        YSpeed += gravity;
    }
    else
    {

        YSpeed -= gravity;
    }
    if( YSpeed <= -maxYSpeed )
    {
        isJump = false;
    }
}

int Sign( float val )
{
    if(val > 0) return 1;
    if(val < 0) return -1;
    return 0;

}

void CPlayer:: adjust(sf::Time deltaTime)
{
    for( int i = 1; i < abs(YSpeed); i++ )
    {
        if( CheckCollision() && !isJump ) //!isJump ����� �� ������� ��������� ���
        {
            sound_jump.play();
            isJump = true;
            YSpeed = 0;
            score +=1;
            break;
        }
        setPosition( getPosition().x , getPosition().y+(Sign(YSpeed)*deltaTime.asSeconds()) );
    }
}

bool CPlayer::CheckCollision()
{
    bool collided = false;

    bottom = getPosition().y + getLocalBounds().height + Sign(YSpeed); // ������� ���, ����� ���������� ������������ �� ������� �����
    left = getPosition().x;
    right = getPosition().x + getLocalBounds().width;
    top = getPosition().y + Sign(YSpeed);

    Rect.left = left;
    Rect.top = top;
    Rect.width = right - left;
    Rect.height = bottom - top;

    for (int i = 0; i < PlatformVector->size( ); i++)
    {
        if (PlatformVector->at(i).Rect.intersects(Rect) && (PlatformVector->at(i).Rect.top >= Rect.top + Rect.height-1) && !isJump && PlatformVector->at(i).index2)
        {
            collided = true;
            if (PlatformVector->at(i).sprindex == 1)
                PlatformVector->at(i).index2 =! PlatformVector->at(i).index2; // ���� ����������� � 2 ��, ������ �� ������
        }
    }

    bottom = getPosition().y + getLocalBounds().height - Sign(YSpeed); //��������� �������
    left = getPosition().x;
    right = getPosition().x + getLocalBounds().width;
    top = getPosition().y - Sign(YSpeed);

    Rect.left = left;
    Rect.top = top;
    Rect.width = right - left;
    Rect.height = bottom - top;

    return collided;
}
