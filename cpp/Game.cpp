#include "..\include\Game.h"
#include "..\include\Player.h"
#include "..\include\Level.h"
#include "..\include\Platform.h"
#include "..\include\Shot.h"
#include "..\include\Enemy.h"
#include "..\include\menu.h"
#include <iostream>
#include <SFML/Audio.hpp>
#include <math.h>
#include <string.h>

//-----------------------------------------------------------------//

using namespace std;

CPlayer o_Player;
Cmenu o_menu;
CLevel o_Level;
vector<CPlatform>PlatformVector(20);

CShot *o_Shot;
CEnemy *o_Enemy;

//-----------------------------------------------------------------//

extern int score;

//-----------------------------------------------------------------//

CGame:: CGame(): Window(sf::VideoMode(400, 600), "Jumper")
    ,ButtonLeft(false)
    ,ButtonRight(false)
    ,ButtonSpace(false)
    ,ButtonMouseLeft(false)
    ,ButtonEscape(false)
    ,CanShot(false)
    ,bulletExist(false)
    ,enemyExist(false)
    ,creatEnemy(true)
    ,pause(false)
    ,ButtonEnter(false)
    ,ButtonTop(false)
    ,ButtonDown(false)
    ,GameOver(false)

//-----------------------------------------------------------------//

{
    Window.setFramerateLimit(60);

    font.loadFromFile("south park.ttf");
    TestString.setFont(font);
    TestString.setCharacterSize(35);
    TestString.setPosition(130,200);
    TestString.setColor(sf::Color::Yellow);
    TestString.setString("Pause");

    FInitPlatform();

    o_Player.PlatformVector = &PlatformVector;

    Texture_shot.loadFromFile("sprite/shot.png");
    Sprite_shoot.setTexture(Texture_shot);

    buffer_shoot.loadFromFile("audio/Shoot.wav");
    sound_shoot.setBuffer(buffer_shoot);
    sound_shoot.setLoop(false);
    sound_shoot.setVolume(35);

    buffer_gameOver.loadFromFile("audio/Game_over.wav");
    buffer_hitEnemy.loadFromFile("audio/Hit_enemy.wav");
    sound_gameOver.setBuffer(buffer_gameOver);
    sound_hitEnemy.setBuffer(buffer_hitEnemy);
    sound_hitEnemy.setVolume(35);

    Texture_Enemy0.loadFromFile("sprite/bad_1.png");
    Sprite_Enemy0.setTexture(Texture_Enemy0);
    Texture_Enemy1.loadFromFile("sprite/bad_2.png");
    Sprite_Enemy1.setTexture(Texture_Enemy1);
    Texture_Enemy2.loadFromFile("sprite/bad_2.png");
    Sprite_Enemy2.setTexture(Texture_Enemy2);

    Texture_platform[0].loadFromFile("sprite/platfom1.png");
    Texture_platform[1].loadFromFile("sprite/platfom2.png");
    Texture_platform[2].loadFromFile("sprite/platfom3.png");

    for( int i = 0; i < 3; i++ )
        Sprite_platform[i].setTexture(Texture_platform[i]);
}

//-----------------------------------------------------------------//

void CGame::Run()
{
    sf::Clock clockPlatform;
    sf::Clock clock;


    while (Window.isOpen())
    {
        time_platform=clock_platform.getElapsedTime();  //��� �������� ����� ������
        sf::Time deltaTime= clock.restart();

        ProcessEvents();
        Update(deltaTime);
        Render();
    }
}

//-----------------------------------------------------------------//

void CGame::ProcessEvents()
{
    sf::Event event;
    while (Window.pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::KeyPressed:
            HandlePlayerInput(event.key.code, true);
            break;

        case sf::Event::KeyReleased:
            HandlePlayerInput(event.key.code, false);
            break;

        case sf::Event::MouseButtonPressed:

            if (CanShot == true);
            CheckMouse(event.mouseButton.button, true);
            break;

        case sf::Event::MouseButtonReleased:
            CheckMouse(event.mouseButton.button, false);
            break;

        case sf::Event::Closed:
            Window.close();
            break;
        }
    }
}


//-----------------------------------------------------------------//

void CGame::Update(sf::Time deltaTime)
{
    if (o_menu.menu)  // �������� ����
    {
        o_menu.Update();
        if (ButtonDown)
        {
            o_menu.select1 =! o_menu.select1;
            ButtonDown =! ButtonDown;
        }
        if (ButtonTop)
        {
            o_menu.select1 =! o_menu.select1;
            ButtonTop =! ButtonTop;
        }

        if (ButtonEnter)
        {
            if (o_menu.select1)
            {
                o_menu.menu = false;
                pause =  false;
                ButtonEnter =!ButtonEnter;
                CanShot = true;
                o_menu.sound_menu.stop();
            }

            if (!o_menu.select1)  // ���� �����
            {
                Window.close();
            }
        }
    }

    if (!o_menu.menu && !GameOver )  // ����
    {
        // o_menu.Write();
        if (ButtonEscape)
            Window.close();

        if (ButtonSpace)
        {
            pause = !pause ;
            ButtonSpace = false;
        }

        if (!pause)
        {
            if (o_menu.menu == true)
                o_menu.Update();

            o_menu.Update_score();

            if (time_enemy.asSeconds()>= rand()%60-20 && enemyExist == false )
            {
                srand (time(NULL));
                o_Enemy = new CEnemy(&Sprite_Enemy0,&Sprite_Enemy1,&Sprite_Enemy2); // ������� �����
                o_Enemy->Sprite_Enemy->setPosition(rand() % 300, -100);
                clock_enemy.restart();
                enemyExist = true;
            }

            if (enemyExist==true)  // ��������� �����
            {
                if (o_Enemy->Rect.intersects(o_Player.Rect))
                {
                    GameOver = true;
                    sound_gameOver.play();
                    pause = !pause ;
                }

                o_Enemy->Update(deltaTime);
                o_Enemy->x = 0;
                o_Enemy->y = 0;

                if( bulletExist )
                    if (o_Shot->Rect.intersects(o_Enemy->Rect))
                    {
                        sound_hitEnemy.play();
                        enemyExist = false;
                        delete o_Enemy;
                        score +=5;
                        delete o_Shot;
                        CanShot = true;
                        bulletExist = false;
                    }
                if (o_Enemy->possitionY >= 620)
                {
                    enemyExist = false;
                    delete o_Enemy;
                }
            }

            if (o_Player.possitionY < 600 )
            {
                reInitPlatform();
                o_Level.Update(deltaTime);
            }

            if (o_Player.possitionY <150 )
            {
                reInitPlatform();
                o_Level.Update(deltaTime);
            }

            o_Player.Update(deltaTime);

            if (ButtonLeft && o_Player.left > 0 )
                o_Player.move(sf::Vector2f(-4,0));

            if (ButtonRight && o_Player.right < 400)
                o_Player.move(sf::Vector2f(+4,0));

            if( bulletExist )  // �������� ��������
            {
                o_Shot->Update(deltaTime);
                time_update_SH = clock_update_SH.getElapsedTime();
                if (time_update_SH.asSeconds() >= 1.5)
                {
                    delete o_Shot;
                    CanShot = true;
                    bulletExist = false;
                    time_update_SH = clock_update_SH.restart();
                }
            }

            if (o_Player.possitionY >=620)
            {
                GameOver = true;
                pause = true;
                sound_gameOver.play();
            }

            for( int i = 0; i < PlatformVector.size( ); i++)
            {
                if (PlatformVector[i].sprindex == 2)
                {
                    initPlatform(i);
                    PlatformVector[i].x += PlatformVector[i].platform_speed;
                    if (time_platform.asSeconds()>=2)
                    {
                        PlatformVector[i].platform_speed = - PlatformVector[i].platform_speed;
                        clock_platform.restart();
                    }
                }
            }
        }
    }

    if (!o_menu.menu && GameOver )  // ���� ���� ����
    {
        o_menu.Update();
        if (ButtonDown)
        {
            o_menu.select2 =! o_menu.select2;
            ButtonDown =! ButtonDown;
        }
        if (ButtonTop)
        {
            o_menu.select2 =! o_menu.select2;
            ButtonTop =! ButtonTop;
        }

        if (ButtonEnter)
        {
            o_menu.Write();
            if (o_menu.select2)  //��� ������ ��������
            {
                GameOver = false;
                pause = false;
                enemyExist = false;
                delete o_Enemy;
                o_Player.setPosition(150,250);
                score = 0;
                FInitPlatform();

            }

            if (!o_menu.select2)  // ��� ������ �������� ����
            {
                Window.close();
            }

        }
    }
}

//-----------------------------------------------------------------//

void CGame::Render()
{
    Window.clear();

    if (o_menu.menu) //�������� ����
    {
        Window.draw(o_menu.sprite);
        Window.draw(o_menu.name);
        Window.draw(o_menu.start);
        Window.draw(o_menu.exit);
    }

    if (!o_menu.menu) // ����
    {
        Window.draw(TestString);

        for (int i = 0; i<4; i++)
            Window.draw(o_Level.Sprite_Level[i]);

        for (int i = 0; i < PlatformVector.size(); i ++)
        {
            if(PlatformVector[i].index2)
            {
                Sprite_platform[PlatformVector[i].sprindex].setPosition( PlatformVector[i].x, PlatformVector[i].y ); /// ������ ������� ���������� ���������.
                Window.draw( Sprite_platform[PlatformVector[i].sprindex]);///������ ������ ���������
            }
        }

        Window.draw(o_Player);
        if (enemyExist ==true)
            Window.draw(*o_Enemy->Sprite_Enemy);

        if (bulletExist == true)
            Window.draw(*o_Shot->Sprite_shoot);

        if (o_Player.GameOver == true)
            Window.draw(o_Player.Sprite_GameOver);

        if (pause)
            Window.draw(TestString);

        Window.draw(o_menu.smallScore);

    }

    if(GameOver == true)
    {
        Window.draw(o_menu.sprite);
        Window.draw(o_menu.textGameOver);
        Window.draw(o_menu.textScore);
        Window.draw(o_menu.best);
        Window.draw(o_menu.restart);
        Window.draw(o_menu.exit2);
    }

    Window.display();
}

//-----------------------------------------------------------------//

void CGame::HandlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
    if (key== sf::Keyboard::Left) ButtonLeft = isPressed;
    if (key== sf::Keyboard::Right) ButtonRight = isPressed;
    if (key== sf::Keyboard::Space) ButtonSpace = isPressed;
    if (key== sf::Keyboard::Escape) ButtonEscape = isPressed;
    if (key== sf::Keyboard::Return) ButtonEnter = isPressed;
    if (key== sf::Keyboard::Down) ButtonDown = isPressed;
    if (key== sf::Keyboard::Up) ButtonTop = isPressed;
}

//-----------------------------------------------------------------//

void CGame::CheckMouse(sf::Mouse::Button button, bool isPressed)
{
    if(!pause)
        if ( CanShot == true )
            GenerationShot();
}

//-----------------------------------------------------------------//

void CGame::GenerationShot()
{
    playerX=o_Player.getPosition().x + 25 ;
    playerY=o_Player.getPosition().y + 3 ;

    o_Shot = new CShot(&Sprite_shoot);
    sound_shoot.play();

    o_Shot->Sprite_shoot->setPosition(playerX,playerY);

    o_Shot->MouseX =  sf::Mouse::getPosition(Window).x;
    o_Shot->MouseY =  sf::Mouse::getPosition(Window).y;
    o_Shot->angle = -atan2((playerY - o_Shot->MouseY), (o_Shot->MouseX - playerX)) * 180 / PI; // ���� ����� �������

    CanShot = false;
    bulletExist = true;
    clock_update_SH.restart();
}

//-----------------------------------------------------------------//

void CGame::initPlatform(int i)
{
    bottom = PlatformVector[i].y + Sprite_platform[PlatformVector[i].sprindex].getLocalBounds().height;
    left = PlatformVector[i].x;
    right = PlatformVector[i].x + Sprite_platform[PlatformVector[i].sprindex].getLocalBounds().width;
    top = PlatformVector[i].y;

    PlatformVector[i].Rect.left = left;
    PlatformVector[i].Rect.top = top;
    PlatformVector[i].Rect.width = right - left;
    PlatformVector[i].Rect.height = bottom - top;
}

//-----------------------------------------------------------------//

void CGame:: reInitPlatform()
{
    for( int i = 0; i < PlatformVector.size( ); i++)
    {
        PlatformVector[i].y +=2;

        if (PlatformVector[i].y >=620)
        {
            PlatformVector[i].index2 = true;
            PlatformVector[i].y =rand() % -70 -30;
            PlatformVector[i].x = rand() % 300;
        }
        bottom = PlatformVector[i].y + Sprite_platform[PlatformVector[i].sprindex].getLocalBounds().height;
        left = PlatformVector[i].x;
        right = PlatformVector[i].x + Sprite_platform[PlatformVector[i].sprindex].getLocalBounds().width;
        top = PlatformVector[i].y;

        PlatformVector[i].Rect.left = left;
        PlatformVector[i].Rect.top = top;
        PlatformVector[i].Rect.width = right - left;
        PlatformVector[i].Rect.height = bottom - top;
    }
}

void CGame::FInitPlatform()
{
    srand (time(NULL));

    for( int i = 0; i < PlatformVector.size( ); i++)
    {
        PlatformVector[i].sprindex = rand()%3;
        PlatformVector[i].index2 = true;
        PlatformVector[i].x = 150;
        PlatformVector[i].y = rand() % 51 + 500 ;
        if (i >= 1)
            PlatformVector[i].y =PlatformVector[i-1].y  - rand() % 41 - 49;
        if (rand()%100 >= 50 ) PlatformVector[i].platform_speed = - PlatformVector[i].platform_speed; // ������������� �������� ��������
    }

    for( int i = 0; i < PlatformVector.size(); i++)
    {
        initPlatform(i);
    }
}
