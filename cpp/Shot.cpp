#include"..\include\Shot.h"
#include <cmath>
#include <iostream>
CShot:: CShot(sf::Sprite *Sprite)
{
    Sprite_shoot = Sprite;
    Rect.left = Sprite->getPosition().x;
    Rect.top = Sprite->getPosition().y;
    Rect.width =Sprite->getGlobalBounds().width;
    Rect.height = Sprite->getGlobalBounds().height;


}

CShot:: ~CShot()
{

}

void CShot:: Update(sf::Time deltaTime)
{
    bottom = Sprite_shoot->getPosition().y + Sprite_shoot->getLocalBounds().height;
    left = Sprite_shoot->getPosition().x;
    right = Sprite_shoot->getPosition().x + Sprite_shoot->getLocalBounds().width;
    top = Sprite_shoot->getPosition().y;

    Rect.left = left;
    Rect.top = top;
    Rect.width = right - left;
    Rect.height = bottom - top;
    Sprite_shoot->move(sf::Vector2f(targetX + cos(angle * PI/ 180) * shotSpeed,targetY + sin(angle * PI/ 180) * shotSpeed )* deltaTime.asSeconds());
}
