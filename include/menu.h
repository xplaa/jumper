#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class Cmenu
{
public:
    Cmenu();
    bool menu;
    bool select1;
    bool select2;
    int keyCode;
    double Dbest;
    bool play = true;

    sf::Texture texture;
    sf::Sprite sprite;

    void Update();
    void Update_score();
    void Read();
    void Write();

    sf::Font font;
    sf::Text name;
    sf::Text start;
    sf::Text exit;
    sf::Text textGameOver;
    sf::Text textScore;
    sf::Text exit2;
    sf::Text restart;
    sf::Text best;
    sf::Text smallScore;

    sf::SoundBuffer buffer_menu;
    sf::Sound sound_menu;
};
