#include <SFML/Graphics.hpp>

class CLevel
{
public:
    CLevel();
    ~CLevel();

    float x,y,y1=0,y2=-599,y3=-1198,y4=-1797;
    float LevelSpeed =0.5;
    sf::Sprite Sprite_Level[4];
    sf::Texture texture[4];

    void Update(sf::Time deltaTime);
};
