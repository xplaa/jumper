#include <SFML/Graphics.hpp>
#include "Platform.h"

class CPlayer: public sf::Sprite
{
public:
    CPlayer();
    ~CPlayer();

    float bottom, left, right, top;

    float PlayerSpeed= 4;
    float gravity = 15.f;
    float YSpeed = 0; // ������������ ��������
    float maxYSpeed = 250; // ������������ ����������� ��������, ��� ������� ����� �������� ������
    float possitionX;
    float possitionY;
    bool isJump = false;
    bool GameOver = false;


    sf::FloatRect Rect;
    sf::Texture Texture_Player;

    sf::Sprite Sprite_GameOver;
    sf::Texture Texture_GameOver;

    sf::Clock clock;
    sf::Time time;

    void adjust(sf::Time deltaTime);
    void Update(sf::Time deltaTime);
    bool CheckCollision();
    std::vector<CPlatform> *PlatformVector;
};
