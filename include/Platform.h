#pragma once
#include <SFML/Graphics.hpp>

class CPlatform
{
public:
    CPlatform();
    ~CPlatform();
    int sprindex;
    bool CanDraw = true;
    bool index2 = true;
    float platform_speed = 1;
    sf::FloatRect Rect;
    sf::IntRect rectangle;
    float x,y;
};
