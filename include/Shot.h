#include <SFML/Graphics.hpp>

class CShot
{
public:
    CShot(sf::Sprite *Sprite);
    ~CShot();
    int shotSpeed = 400;
    void Update(sf::Time deltaTime);
    const double PI = 3.14159265;
    sf::FloatRect Rect;
    float bottom, left, right, top;

    float MouseX, MouseY;
    float x,y;
    float targetX = 0, targetY = 0;
    float angle = 0;

    sf::Sprite *Sprite_shoot;
};


