#include <SFML/Graphics.hpp>

class CEnemy
{
public:
    CEnemy(sf::Sprite *Sprite1,sf::Sprite *Sprite2,sf::Sprite *Sprite3);
    ~CEnemy();

    float EnemySpeed=450, x, y =0, possitionY;
    float bottom, left, right, top;
    int index;
    sf::FloatRect Rect;

    void Update(sf::Time deltaTime);
    sf::Sprite *Sprite_Enemy;
};
