#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class CGame
{
public:
    CGame();
    void Run();
private:
    void initPlatform(int i);
    void Update(sf::Time elapsedTime);
    void Update_shot(sf::Time elapsedTime2);
    void ProcessEvents();
    void Render();
    void HandlePlayerInput(sf::Keyboard::Key key, bool isPressed);
    void CheckMouse(sf::Mouse::Button button,bool isPressed);
    bool IsColliding(sf::RectangleShape rectA, sf::RectangleShape rectB);
    void reInitPlatform();
    void GenerationShot();
    void FInitPlatform();

    sf::Time timeToShot;
    sf::Time time_update_SH;
    sf::Time time_platform;
    sf::Time time_enemy;

    sf::Clock clockToShot;
    sf::Clock clock_enemy;
    sf::Clock clock_update_SH;
    sf::Clock clock_platform;

    sf::RenderWindow Window;

    sf::Text TestString;

    sf::Font font;

    sf::Texture Texture_shot;
    sf::Texture Texture_Enemy0;
    sf::Texture Texture_Enemy1;
    sf::Texture Texture_Enemy2;

    sf::Sprite Sprite_shoot;
    sf::Sprite Sprite_Enemy0;
    sf::Sprite Sprite_Enemy1;
    sf::Sprite Sprite_Enemy2;

    sf::SoundBuffer buffer_shoot;
    sf::SoundBuffer buffer_gameOver;
    sf::SoundBuffer buffer_hitEnemy;

    sf::Sound sound_shoot;
    sf::Sound sound_gameOver;
    sf::Sound sound_hitEnemy;

    sf::Texture Texture_platform[3];
    sf::Sprite Sprite_platform[3];

    float playerX, playerY;
    float bottom, left, right, top;
    const double PI = 3.14159265;

    bool ButtonMouseLeft;
    bool ButtonLeft;
    bool ButtonRight;
    bool ButtonSpace;
    bool ButtonEscape;
    bool ButtonEnter;
    bool ButtonTop;
    bool ButtonDown;
    bool GameOver;
    bool bulletExist;
    bool enemyExist;
    bool CanShot;
    bool creatEnemy;
    bool pause;

};
